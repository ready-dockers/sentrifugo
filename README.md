# Sentrifugo
Sentrifugo is a Human Resource Management System

## config
database `MYSQL_ROOT_PASSWORD` and `MYSQL_DATABASE` configure into docker-compose.yml file.
Additionlay `MYSQL_USER`, `MYSQL_PASSWORD` can be set to create new user.
(**Note**: Database Host is db and User name is root )

## run 
```sh
docker-compose up
```
Verfiry the deployment by navigating to you server address in your prefered browser.
```sh
http://localhost/sentrifugo
```

## version
version 3.2


